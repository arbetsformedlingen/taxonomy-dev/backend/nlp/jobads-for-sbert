import json

dataset_filename = "tiny_dataset.jsonl"

# Load the dataset
# Each line in the file is one json-encoded jobad.
# See https://stackoverflow.com/a/56749504 for how to decode that.

with open(dataset_filename, 'r') as json_file:
    result = [json.loads(json_str) for json_str in list(json_file)]

# Display som example information from the dataset
print("There are {:d} records in the file".format(len(result)))

x = result[4]


print("Take for instance the ad with id {:s}".format(x["external_id"]))
print("It has following text: {:s} ...".format(x["description"]["text"][0:100]))



keywords = x["keywords"]["enriched"]
print("It contains the following keywords related to occupation and skill: {:s}".format(", ".join(set(keywords["occupation"] + keywords["skill"]))))


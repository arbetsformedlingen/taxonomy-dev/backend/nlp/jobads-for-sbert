# jobads-for-sbert

Det här är ett startprojekt för att komma igång med kod för att skapa träningsdata för [Sentence-BERT](https://arxiv.org/abs/1908.10084). Men om du vill skapa ett helt eget projekt från grunden går det naturligtvis också bra.

## Bakgrund

Bakgrunden till det här projektet är att vi vill finjustera Sentence-BERT för arbetsmarknadsdata. Sentence-BERT är en algoritm som omvandlar en godtycklig mening eller text till en numerisk representation i form av en vektor som ofta är av dimensionen 768. Sentence-BERT bygger på [BERT](https://arxiv.org/abs/1810.04805v2) som är ett neuralt nätverk för att koda om text till numeriska representationer. BERT lär sig denna kodning baserat på träningsdata i form av exempel som visar hur algoritmen ska bete sig.

Tanken med det här projektet är att producera träningsdata för Sentence-BERT. Hur träningen av Sentence-BERT går till förklaras i korthet i *avsnitt 3* i [Sentence-BERT-artikeln](https://arxiv.org/abs/1908.10084) men mer ingående i *figur 3* och *avsnitt 3.1* i den refererade artikeln om [FaceNet](https://arxiv.org/pdf/1503.03832.pdf).

Exemplen kan i huvudsak vara antingen (i) *tripler av text* eller (ii) *par av text tillsammans med ett numeriskt mått på hur lika de är*.

Vi kan börja med att betrakta triplerna. Varje sådant exempel bygger på tre stycken text. Här är ett exempel på en tripel med koppling till botanik:

* ett så kallat **ankare**, t.ex. "gran"
* ett så kallat **positivt exempel**, t.ex. "tall"
* ett så kallat **negativt exempel**, t.ex. "blåklocka"

Tanken är att betydelsen av det positiva exemplet ligger närmre ankarens betydelse än vad det negativa exemplet gör. I ovanstående exempel är det naturligt att "gran" och "tall" är närmre varandra än vad "gran" och "blåklocka" är, eftersom gran och tall båda är träd som dessutom har barr. Genom att träna Sentence-BERT med en stor mängd sådana exempel kommer att Sentence-BERT att lära sig att koda text på ett sätt som avspeglar textens betydelse, så att kodningen av "gran" och "tall" är närmre varandra än vad "gran" och "blåklocka" är.

## Producera arbetsmarknadsdata för SBERT 

Tanken är nu att vi ska försöka producera tripler för arbetsmarknaden. Varje sådan tripler ska bestå av tre jobbannonser:

* Ankare: En slumpmässigt vald jobbannons, t.ex. en platsannons för att jobba som redovisningskonsult.
* Positivt exempel: En jobbannons som visserligen är slumpmässigt vald, men med villkoret att den ska *ligga nära* ankaret. Exempelvis en platsannons för att jobba som löneadministratör som också är ett kontorsjobb som har med ekonomi att göra.
* Ett negativt exempel: En jobbannons som ligger längre ifrån "redovisningskonsult" än vad "löneadministratör" gör. Det skulle kunna vara något utomhusarbete som inte har med ekonomi att göra, förslagsvis en annons för trädgårdsarbete.

För att bygga tripler av ovanstående typ skulle en metod kunna vara att generera ett stort antal tripler helt slumpmässigt och mata ut dem i ett kalkylblad med tre kolumner (`ankare`, `positivt exempel`, `negativt exempel`) och låta en människa för hand gå igenom varje exempel och fylla i en fjärde kolumn (`behåll?`) med 1 eller 0 för huruvida exemplet är bra eller ej. Men det skulle vara arbetsamt.

Mitt förslag är istället att vi definierar ett ungefärligt *mått på hur lika* två jobbannonser är baserat på vilka nyckelord de innehåller. Det finns många mått men man kanske kan använda [Jaccard-index](https://en.wikipedia.org/wiki/Jaccard_index#Weighted_Jaccard_similarity_and_distance): Antag att den ena annonsen innehåller en mängd *A = {trädgård, klippa buskar}* och den andra annonsen innehåller mängden *B = {trädgård, skottkärra}*. Då skulle man kunna definiera ett mått av överlapp i betydelse som `likhet = |snitt(A, B)| / |union(A, B)| = |{trädgård}| / |{trådgård, klippa buskar, skottkärra}| = 1/3`, där `|...|` betyder mängdens kardinalitet, d.v.s. hur många element den innehåller. Dessa nyckelord finns färdigextraherade och kan laddas ner som zip-filer härifrån: https://data.jobtechdev.se/annonser/historiska/berikade/kompletta/.

Målet med det här projektet är att generera tripler sådana att ett positivt exempel ligger närmre ankaret än ett negativt exempel baserat på ovanstående likhetsmått eller något annat mått.

## Grov planering steg-för-steg

Här är *förslag* på hur du kan gå tillväga för att generera träningsdata men du måste inte följa dessa steg :-)

1. Börja med att ladda ned det här projektet *eller* skapa en ny mapp på datorn där du lägger projektet.
2. Kontrollera att du har en fungerande Python-miljö som du är bekväm med (Python3-interpretatorn, en textredigerare t.ex. VSCode).
3. För att komma igång, kör skriptet `main.py` genom att anropa `python3 main.py` från kommandoraden eller genom att köra det inifrån din utvecklingsmiljö. Detta skript laddar in ett litet demo-dataset och visar lite information.
4. Du kan ladda ner ett stort dataset att arbeta med istället, från https://data.jobtechdev.se/annonser/historiska/berikade/kompletta/. Men när du bara provkör kod under utvecklingen kanske det är mer praktiskt att provköra på `tiny_dataset.jsonl`.
5. Skriv en funktion som, givet en jobbannons ur datamängden, returnerar alla nyckelord av typen `skill` och `occupation`. Det är lämpligt att returnera dessa som en *mängd* i Python, d.v.s. [`set`](https://docs.python.org/3/tutorial/datastructures.html#sets).
6. Skriv en funktion som givet två sådana mängder beräknar någon form av likhet t.ex. grad av överlapp (`|snitt(A, B)|/|union(A, B)|` d.v.s. Jaccard-index).
7. Välj en slumpmässig jobbannons som *ankare*. Välj sedan ett slumpmässigt urval av *andra* annonser. För varje sådan annons, beräkna hur lik den är i jämförelse med ankaret och välj sedan en av dessa med *stor likhet* mot ankaret som positivt exempel och en annan av dessa me d*liten likhet* mot ankaret som negativt exempel.
8. Upprepa steg 6 flera gånger (kanske 10000 gånger?) och spara alla de slumpmässiga exemplen i t.ex. en [CSV-fil]() med kolumnerna `anchor`, `positive`, `negative`, där du sparar `external-id` för varje annons. Du kan även inkludera kolumner `positive-similarity` och `negative-similarity` för hur mycket det positiva och negativa exemplet matchar ankaret. Om du vill kanske även kolumner för annonstiteln för varje annons i trippeln, så att det är lätt att se vilken typ av jobb det gäller.
9. Titta på resultatet i CSV-filen. Det är praktiskt att öppna CSV-filen med något kalkylprogram, t.ex. Excel eller LibreOffice Calc. Verkar exemplen rimliga? Om inte kan du försöka justera algoritmen lite.
